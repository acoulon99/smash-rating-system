import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;


public class MasterPlayerList {

ArrayList<Player> playerlist = new ArrayList<Player>();
File playerfile;

	public MasterPlayerList(File playerfile) throws NumberFormatException, IOException {
		this.playerfile = playerfile;
		
		// loads the existing master player list
		LoadMasterPlayerList(this.playerfile);
	}
	
	
	void UpdateEloRating(Player winner, Player looser) {
		if (winner == null || looser == null) {
			return;  // the match is either a bye or empty
		}
		double K = 32;
		
		// calculate the expected score for the winner and looser
		double part = (looser.rating - winner.rating) / 400.0;
		double winnerexpect = 1 / (1 + Math.pow(10, part));
		double looserexpect = 1 - winnerexpect;
		
		
		// update rankings based on expected score
		winner.rating = winner.rating + K * (1 - winnerexpect);
		looser.rating = looser.rating + K * (0 - looserexpect);
		
		System.out.println("Player: " + winner.tag + " || New Rating: " + winner.rating);
		System.out.println("Player: " + looser.tag + " || New Rating: " + looser.rating);
		
		
	}
	
	void AddPlayer(Player player) {
		boolean isnewplayer = true;
		
		// searches through the player list to see if he/she is already added
		for (Player oldplayer : this.playerlist) {
			if (oldplayer.tag.equals(player.tag)) {
				isnewplayer = false;
			}
		}
		
		// if a new player, adds them
		if (isnewplayer) {
			System.out.println("Adding: " + player.tag);
			this.playerlist.add(player);
		}
	}
	
	void LoadMasterPlayerList(File playerfile) throws NumberFormatException, IOException {
		
		// creates a reader to go through the player file
		BufferedReader reader = new BufferedReader(new FileReader(playerfile));
		String rawline;
		String[] line;
		System.out.println("Reading current player list");
		
		// loops through each player in the player file
		while ((rawline = reader.readLine()) != null) {
			line = rawline.split(",");
			
			// extracts player information
			int ID = Integer.parseInt(line[0]);
			String tag = line[1];
			String name = line[2];
			String location = line[3];
			double rating = Double.parseDouble(line[4]);
			String main = line[5];
			
			// creates a new player instance
			Player player= new Player(ID, tag, name, location, rating, main);
			
			System.out.println("Loading: " + player.tag + " (" + player.name + ")");
			
			// Adds the player to the master list
			this.playerlist.add(player);
		}
		
		// time to say goodbye
		reader.close();
	}
	
	void WriteMasterPlayerList(File playerfile) throws IOException {
		
		//File thefile = new File(playerfile);
		
		// sets up a writer to write to the newly created file
		BufferedWriter writer = new BufferedWriter(new FileWriter(playerfile));
		
		// for each player in the list, writes their updated information		
		for (Player player : this.playerlist) {
			writer.write(player.ID + ",");
			writer.write(player.tag + ",");
			writer.write(player.name + ",");
			writer.write(player.location + ",");
			writer.write(Double.toString(player.rating) + ",");
			writer.write(player.main);
			writer.newLine();
		}
				
		// Gandalf: You shall not write more!
		writer.close();
		
	}
}
