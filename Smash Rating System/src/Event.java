import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class Event {
	
	public ArrayList<String[]> playerTID = new ArrayList<String[]>();
	public File newtournamentfile;
	public MasterPlayerList masterplayerlist;
	
	
	public Event(File newtournamentfile, MasterPlayerList masterplayerlist) {
		this.masterplayerlist = masterplayerlist;
		this.newtournamentfile = newtournamentfile;
		System.out.println(this.newtournamentfile);
	}
	
	
	Player PlayerTagLookup(String playertag) {
		
		for (Player player : this.masterplayerlist.playerlist) {
			if (player.tag.equals(playertag)) {
				return player;
			}
		}
		
		return null;
	}
	
	
	String[] TagTIDlookup(String winnerTID, String player1TID, String player2TID) {
		String[] winnerlooser = {null, null};
	
		String looserTID;
		
		// determine the looser TID
		if (player1TID.equals(winnerTID)) {
			looserTID = player2TID;
		}
		else {
			looserTID = player1TID;
		}
		
		for (String[] pair : this.playerTID) {
			
			if ( pair[1].equals(winnerTID) ) {
				winnerlooser[0] = pair[0];     // finds the winner's tag
			}
			if (pair[1].equals(looserTID) ) {
				winnerlooser[1] = pair[0];    // finds the looser's tag
			}
		}
		
		return winnerlooser;
	}

	void UpdatePlayerRanks() throws ParserConfigurationException, SAXException, IOException {
		
		// sets up the builder parser to parse the input stream
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
						
		// creates the inputstream of the xml file
		InputStream stream = new FileInputStream(this.newtournamentfile);
						
		// parses the inputstream into a document
		Document document = builder.parse(stream);
						
		// makes a list of all entrant nodes
		NodeList entrantnodes = document.getElementsByTagName("Player");
		NodeList matchnodes = document.getElementsByTagName("Match");
		
		// loops through each entrant finding their TID
		for (int i=0; i < entrantnodes.getLength(); i++) {
			String[] pair = new String[2];
			
			Element entrant = (Element) entrantnodes.item(i);
							
			// accesses the nickname
			NodeList nickname = entrant.getElementsByTagName("Nickname");
			NodeList TID = entrant.getElementsByTagName("ID");
			Element line1 = (Element) nickname.item(0);
			Element line2 = (Element) TID.item(0);
			
			pair[0] = line1.getTextContent();  // tag
			pair[1] = line2.getTextContent();  // TID
					
			this.playerTID.add(pair);
		}
		
		
		// loops through each match in the event and determines winner and looser
		for (int i=0; i < matchnodes.getLength(); i++) {
			String[] winnerlooser = new String[2];
			
			Element match = (Element) matchnodes.item(i);
					
			//outputs match winners
			NodeList winner = match.getElementsByTagName("Winner");
			NodeList player1 = match.getElementsByTagName("Player1");
			NodeList player2 = match.getElementsByTagName("Player2");
					
			Element linewinner = (Element) winner.item(0);
			Element lineplayer1 = (Element) player1.item(0);
			Element lineplayer2 = (Element) player2.item(0);
					
			String winnerTID = linewinner.getTextContent();
			String player1TID = lineplayer1.getTextContent();
			String player2TID = lineplayer2.getTextContent();
			
			winnerlooser = TagTIDlookup(winnerTID, player1TID, player2TID); // list containing {winner,looser} tag names
			
			Player winnerplayer = PlayerTagLookup(winnerlooser[0]);
			Player looserplayer = PlayerTagLookup(winnerlooser[1]);
			
			this.masterplayerlist.UpdateEloRating(winnerplayer, looserplayer);
		}
	}
	
	void ExtractAddPlayers() throws ParserConfigurationException, SAXException, IOException {
		
		// sets up the builder parser to parse the input stream
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
				
		// creates the inputstream of the xml file
		InputStream stream = new FileInputStream(this.newtournamentfile);
				
		// parses the inputstream into a document
		Document document = builder.parse(stream);
				
		// makes a list of all entrant nodes
		NodeList entrantnodes = document.getElementsByTagName("Player");
				
		// loops through each entrant
		for (int i=0; i < entrantnodes.getLength(); i++) {
			Element entrant = (Element) entrantnodes.item(i);
					
			// accesses the nickname
			NodeList nickname = entrant.getElementsByTagName("Nickname");
			Element line = (Element) nickname.item(0);
			
			// create a player instance of the tournament entrant 
			Player player = new Player(100, line.getTextContent(), "unknown", "unknown", 1500, "unknown");
			this.masterplayerlist.AddPlayer(player);
		}
	}
}
