import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.filechooser.*;
import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class RatingSystem extends JPanel implements ActionListener{
	static private final String newline = "\n";
	JButton playerlistButton, tournamentButton;
	JTextArea log;
	JFileChooser fc;
	MasterPlayerList masterplayerlist;
	File playerfile;
	
	public RatingSystem() {
		super(new BorderLayout());
		
		// creates the log
		log = new JTextArea(5,20);
		log.setMargin(new Insets(5,5,5,5));
		log.setEditable(false);
		JScrollPane logScrollPane = new JScrollPane(log);
		
		// creates the file chooser
		fc = new JFileChooser();
		
		// create open and save buttons
		playerlistButton = new JButton("Select Master Player List...", new ImageIcon("media/Open16.gif"));
		playerlistButton.addActionListener(this);
		tournamentButton = new JButton("Select a TioPro Tournament File...", new ImageIcon("media/Open16.gif"));
		tournamentButton.addActionListener(this);
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.add(playerlistButton);
		buttonPanel.add(tournamentButton);
		
		//Add the buttons and log to the panel
		add(buttonPanel, BorderLayout.PAGE_START);
		add(logScrollPane, BorderLayout.CENTER);
	}
	
	public void actionPerformed(ActionEvent e) {
		
		// handle the open button action
		if (e.getSource() == playerlistButton) {
			int returnVal = fc.showOpenDialog(RatingSystem.this);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				this.playerfile = fc.getSelectedFile();
				// this is where the application would open the file
				
				try {
					this.masterplayerlist = new MasterPlayerList(playerfile);
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				
				log.append("Opening: " + this.playerfile.getName() + newline);
			} else {
				log.append("Loading player list cancelled by user." + newline);
			}
			log.setCaretPosition(log.getDocument().getLength());
		
		// handle the save button action
		} else if (e.getSource() == tournamentButton) {
			int returnVal = fc.showOpenDialog(RatingSystem.this);
			
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				Event event = new Event(file, this.masterplayerlist);
				try {
					event.ExtractAddPlayers();
				} catch (ParserConfigurationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					event.UpdatePlayerRanks();
				} catch (ParserConfigurationException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SAXException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					masterplayerlist.WriteMasterPlayerList(this.playerfile);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				log.append("Opening: " + file.getName() + newline);
			} else {
				log.append("Loading tournament file cancelled by user." + newline);
			}
			log.setCaretPosition(log.getDocument().getLength());
		}
	}
	
	
	protected static ImageIcon createImageIcon(String path) {
		java.net.URL imgURL = RatingSystem.class.getResource(path);
		System.out.println(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't fine file: " + path);
			return null;
		}
		
	}
	
	
	private static void createAndShowGUI() {
		// Create and set up the window
		JFrame frame = new JFrame("FileChooserDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// Add content to the window
		frame.add(new RatingSystem());
		
		// Display the window
		frame.pack();
		frame.setVisible(true);
		
	}

	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
		System.out.println("hello smashers");
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				//Turn off metal's use of bold fonts
				UIManager.put("swing.boldMetal", Boolean.FALSE);
				createAndShowGUI();
			}
		});
		
		// location of the player and tournament lists
		//String playerfileloc = "media/masterplayerlist.csv";
		
		//String newtournamentfile = "media/TO9 Project M.tio";
		
		
		
		// INITIALIZE
		// create master player list instance
		//String playerfileloc = JOptionPane.showInputDialog("Enter MasterPlayerList file location: ");
		//System.out.println("Loading master player list...");
		//MasterPlayerList masterplayerlist = new MasterPlayerList(playerfileloc);
		

		// create an event instance from the tournament file
		//String newtournamentfile = JOptionPane.showInputDialog("Enter New Tournament file location: ");
		//Event event = new Event(newtournamentfile, masterplayerlist);
		
		// extract the players and then update the matches
		//event.ExtractAddPlayers();
		//event.UpdatePlayerRanks();
		
		// OUTPUT
		// writes out the new master player list
		//masterplayerlist.WriteMasterPlayerList(playerfileloc);
	}
}
