
public class Player {
	public int ID;
	public String tag;
	public String name;
	public String location;
	public String main;
	public double rating;
	
	public Player (int ID, String tag, String name, String location, double rating, String main) {
		this.ID = ID;
		this.tag = tag;
		this.name = name;
		this.location = location;
		this.rating = rating;
		this.main = main;
	}
	

}
